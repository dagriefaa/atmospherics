﻿using UnityEngine;
using spaar.ModLoader.Internal.Tools;
using System.Collections.Generic;
using Modding.Blocks;

public class Mapper : MonoBehaviour {

    // Use this for initialization
    void Awake() {
        List<ComponentEntry.FieldMapping> mappings = new List<ComponentEntry.FieldMapping>();
        mappings.Add(new ComponentEntry.FieldMapping("SkyboxBehaviour.Skyboxes", typeof(Dictionary<string, Cubemap>), (c) => (c as SkyboxController).skyboxTextures.Keys, null));
        mappings.Add(new ComponentEntry.FieldMapping("SkyboxBehaviour.CurrentSkybox", typeof(string), (c) => (c as SkyboxController).currentKey, (c, x) => (c as SkyboxController).SetSkybox((string)x)));
        mappings.Add(new ComponentEntry.FieldMapping("SkyboxBehaviour.SkyboxTint", typeof(Color), (c) => (c as SkyboxController).SkyboxTint, (c, x) => (c as SkyboxController).SkyboxTint = (Color)x));
        mappings.Add(new ComponentEntry.FieldMapping("SkyboxBehaviour.SkyboxRotation", typeof(float), (c) => (c as SkyboxController).SkyboxRotation, (c, x) => (c as SkyboxController).SkyboxRotation = (float)x));
        mappings.Add(new ComponentEntry.FieldMapping("SkyboxBehaviour.SkyboxExposure", typeof(float), (c) => (c as SkyboxController).SkyboxExposure, (c, x) => (c as SkyboxController).SkyboxExposure = (float)x));
        mappings.Add(new ComponentEntry.FieldMapping("SkyboxBehaviour.AmbientIntensity", typeof(float), c => (c as SkyboxController).AmbientIntensity, (c, x) => (c as SkyboxController).AmbientIntensity = (float)x));
        ComponentEntry.AddComponentMappings(typeof(SkyboxController), "Atmospherics", mappings);

        mappings.Clear();
        mappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.Status", typeof(int), c => (c as OpenFogVolume).initialised, null));
        mappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.Color", typeof(Color), (c) => (c as OpenFogVolume).FogColor,
            (c, x) => { (c as OpenFogVolume).FogColor = (Color)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.FogStartDistance", typeof(float), (c) => (c as OpenFogVolume).FogStartDistance,
            (c, x) => { (c as OpenFogVolume).FogStartDistance = (float)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.Visibility", typeof(float), (c) => (c as OpenFogVolume).Visibility,
            (c, x) => { (c as OpenFogVolume).Visibility = (float)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.InscatteringEnabled", typeof(bool), (c) => (c as OpenFogVolume).EnableInscattering,
            (c, x) => { (c as OpenFogVolume).EnableInscattering = (bool)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.InscatteringRotation", typeof(Vector3), (c) => (c as OpenFogVolume).InscatteringRotation,
            (c, x) => { (c as OpenFogVolume).InscatteringRotation = (Vector3)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.InscatteringColor", typeof(Color), (c) => (c as OpenFogVolume).InscatteringColor,
            (c, x) => { (c as OpenFogVolume).InscatteringColor = (Color)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.InscatteringTransitionWideness", typeof(float), (c) => (c as OpenFogVolume).InscatteringTransitionWideness,
            (c, x) => { (c as OpenFogVolume).InscatteringTransitionWideness = (float)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.InscatteringStartDistance", typeof(float), (c) => (c as OpenFogVolume).InscatteringStartDistance,
            (c, x) => { (c as OpenFogVolume).InscatteringStartDistance = (float)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.InscatteringIntensity", typeof(float), (c) => (c as OpenFogVolume).InscatteringIntensity,
            (c, x) => { (c as OpenFogVolume).InscatteringIntensity = (float)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.InscatteringExponent", typeof(float), (c) => (c as OpenFogVolume).InscatteringExponent,
            (c, x) => { (c as OpenFogVolume).InscatteringExponent = (float)x; }));
        ComponentEntry.AddComponentMappings(typeof(OpenFogVolume), "Atmospherics", mappings);

        mappings.Clear();
        mappings.Add(new ComponentEntry.FieldMapping("FogController.Color", typeof(Color), (c) => FogController.FogColor,
            (c, x) => { FogController.FogColor = (Color)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("FogController.FogStartDistance", typeof(float), (c) => FogController.FogStartDistance,
            (c, x) => { FogController.FogStartDistance = (float)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("FogController.Visibility", typeof(float), (c) => FogController.Visibility,
            (c, x) => { FogController.Visibility = (float)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("FogController.InscatteringEnabled", typeof(bool), (c) => FogController.EnableInscattering,
            (c, x) => { FogController.EnableInscattering = (bool)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("FogController.InscatteringRotation", typeof(Vector3), (c) => FogController.InscatteringRotation,
            (c, x) => { FogController.InscatteringRotation = (Vector3)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("FogController.InscatteringColor", typeof(Color), (c) => FogController.InscatteringColor,
            (c, x) => { FogController.InscatteringColor = (Color)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("FogController.InscatteringTransitionWideness", typeof(float), (c) => FogController.InscatteringTransitionWideness,
            (c, x) => { FogController.InscatteringTransitionWideness = (float)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("FogController.InscatteringStartDistance", typeof(float), (c) => FogController.InscatteringStartDistance,
            (c, x) => { FogController.InscatteringStartDistance = (float)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("FogController.InscatteringIntensity", typeof(float), (c) => FogController.InscatteringIntensity,
            (c, x) => { FogController.InscatteringIntensity = (float)x; }));
        mappings.Add(new ComponentEntry.FieldMapping("FogController.InscatteringExponent", typeof(float), (c) => FogController.InscatteringExponent,
            (c, x) => { FogController.InscatteringExponent = (float)x; }));
        ComponentEntry.AddComponentMappings(typeof(FogController), "Atmospherics", mappings);

        //mappings.Clear();
        //mappings.Add(new ComponentEntry.FieldMapping("AtmosphericsController.AirDensity", typeof(float), c => AtmosphericsController.Gravity, null));
        //mappings.Add(new ComponentEntry.FieldMapping("AtmosphericsController.AirFalloff", typeof(bool), c => AtmosphericsController.AirFalloff, (c, x) => AtmosphericsController.AirFalloff = (bool)x));
        //mappings.Add(new ComponentEntry.FieldMapping("AtmosphericsController.GravityFalloff", typeof(bool), c => AtmosphericsController.GravityFalloff, (c, x) => AtmosphericsController.GravityFalloff = (bool)x));
        //mappings.Add(new ComponentEntry.FieldMapping("AtmosphericsController.IceFreeze", typeof(bool), c => AtmosphericsController.IceFreeze, (c, x) => AtmosphericsController.IceFreeze = (bool)x));
        //mappings.Add(new ComponentEntry.FieldMapping("AtmosphericsController.StartAtmosphereAltitude", typeof(float), c => AtmosphericsController.StartAtmosphereAltitude, (c, x) => AtmosphericsController.StartAtmosphereAltitude = (float)x));
        //mappings.Add(new ComponentEntry.FieldMapping("AtmosphericsController.EndAtmosphereAltitude", typeof(float), c => AtmosphericsController.EndAtmosphereAltitude, (c, x) => AtmosphericsController.EndAtmosphereAltitude = (float)x));
        //mappings.Add(new ComponentEntry.FieldMapping("AtmosphericsController.StartGravityAltitude", typeof(float), c => AtmosphericsController.StartGravityAltitude, (c, x) => AtmosphericsController.StartGravityAltitude = (float)x));
        //mappings.Add(new ComponentEntry.FieldMapping("AtmosphericsController.EndGravityAltitude", typeof(float), c => AtmosphericsController.EndGravityAltitude, (c, x) => AtmosphericsController.EndGravityAltitude = (float)x));
        //ComponentEntry.AddComponentMappings(typeof(AtmosphericsController), "Atmospherics", mappings);

        mappings.Clear();
        mappings.Add(new ComponentEntry.FieldMapping("Light.Rotation", typeof(Vector3), (c) => MainLightController.Rotation, (c, x) => MainLightController.Rotation = (Vector3)x));
        mappings.Add(new ComponentEntry.FieldMapping("Light.Colour", typeof(Color), (c) => MainLightController.Color, (c, x) => MainLightController.Color = (Color)x));
        mappings.Add(new ComponentEntry.FieldMapping("Light.Intensity", typeof(float), (c) => MainLightController.Intensity, (c, x) => MainLightController.Intensity = (float)x));
        mappings.Add(new ComponentEntry.FieldMapping("Light.AmbientColour", typeof(Color), (c) => MainLightController.AmbientColor, (c, x) => MainLightController.AmbientColor = (Color)x));
        mappings.Add(new ComponentEntry.FieldMapping("Light.Flares", typeof(Dictionary<string, Flare>), (c) => MainLightController.LensFlares.Keys, null));
        mappings.Add(new ComponentEntry.FieldMapping("Light.CurrentFlare", typeof(string), (c) => MainLightController.mainLight.flare ? MainLightController.mainLight.flare.name : "null",
            (c, x) => MainLightController.SetFlare((string)x)));
        ComponentEntry.AddComponentMappings(typeof(MainLightController), "Atmospherics", mappings);

        Destroy(this);
    }
}

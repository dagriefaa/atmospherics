﻿using System;
using UnityEngine;

namespace spaar.ModLoader.UIExternal {
    // Token: 0x02000073 RID: 115
    public class Tools {
        // Token: 0x0600036B RID: 875 RVA: 0x0000ED31 File Offset: 0x0000CF31
        public bool DoCollapseArrow(bool isExpanded, bool enabled, params GUILayoutOption[] options) {
            return this.DoCollapseArrow(isExpanded, enabled, Elements.Settings.LogEntrySize, Elements.Settings.LogEntrySize, options);
        }

        // Token: 0x0600036C RID: 876 RVA: 0x0000ED50 File Offset: 0x0000CF50
        public bool DoCollapseArrow(bool isExpanded, params GUILayoutOption[] options) {
            return this.DoCollapseArrow(isExpanded, true, options);
        }

        // Token: 0x0600036D RID: 877 RVA: 0x0000ED5C File Offset: 0x0000CF5C
        public bool DoCollapseArrow(bool isExpanded, bool enabled, float width, float height, params GUILayoutOption[] options) {
            int num = options.Length;
            Array.Resize<GUILayoutOption>(ref options, options.Length + 2);
            options[num] = GUILayout.Width(width);
            options[num + 1] = GUILayout.Height(height);
            GUIStyle style = enabled ? (isExpanded ? Elements.Buttons.ArrowExpanded : Elements.Buttons.ArrowCollapsed) : (isExpanded ? Elements.Buttons.ArrowDarkExpanded : Elements.Buttons.ArrowDarkCollapsed);
            return GUILayout.Button("", style, options) && enabled;
        }

        // Token: 0x0600036E RID: 878 RVA: 0x0000EDD9 File Offset: 0x0000CFD9
        public void Indent(int depth = 1) {
            GUILayout.Space(Elements.Settings.TreeEntryIndention * (float)depth);
        }
    }
}

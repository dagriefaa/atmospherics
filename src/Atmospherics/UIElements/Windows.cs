﻿using System;
using UnityEngine;
using Modding;

namespace spaar.ModLoader.UIExternal {
    // Token: 0x02000075 RID: 117
    public class Windows {
        // Token: 0x1700008E RID: 142
        // (get) Token: 0x06000377 RID: 887 RVA: 0x0000EF7C File Offset: 0x0000D17C
        // (set) Token: 0x06000378 RID: 888 RVA: 0x0000EF84 File Offset: 0x0000D184
        public GUIStyle Default { get; set; }

        // Token: 0x1700008F RID: 143
        // (get) Token: 0x06000379 RID: 889 RVA: 0x0000EF8D File Offset: 0x0000D18D
        // (set) Token: 0x0600037A RID: 890 RVA: 0x0000EF95 File Offset: 0x0000D195
        public GUIStyle ClearDark { get; set; }

        // Token: 0x0600037B RID: 891 RVA: 0x0000EFA0 File Offset: 0x0000D1A0
        internal Windows() {
            this.Default = new GUIStyle {
                normal =
                {
                    background = ModResource.GetTexture("background-44px.png"),
                    textColor = Elements.Colors.DefaultText
                },
                fontSize = 16,
                fontStyle = FontStyle.Bold,
                border = new RectOffset(4, 4, 44, 4),
                padding = new RectOffset(12, 12, 56, 12),
                contentOffset = new Vector2(0f, -42f)
            };
            this.ClearDark = new GUIStyle {
                normal =
                {
                    background = ModResource.GetTexture("background-dark.png"),
                    textColor = Elements.Colors.DefaultText
                },
                border = new RectOffset(4, 4, 4, 4),
                padding = Elements.Settings.DefaultPadding,
                margin = Elements.Settings.DefaultMargin
            };
        }
    }
}

﻿using System;
using UnityEngine;

namespace spaar.ModLoader.UIExternal {
    // Token: 0x0200006B RID: 107
    public static class ColorUtil {
        // Token: 0x06000316 RID: 790 RVA: 0x0000E440 File Offset: 0x0000C640
        public static Color FromRGB255(float r, float g, float b) {
            return new Color(r / 255f, g / 255f, b / 255f);
        }
    }
}

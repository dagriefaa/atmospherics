﻿using UnityEngine;
using System.Collections;

public class ForceBehaviour : MonoBehaviour {

    Rigidbody rigidbody = null;
    AxialDrag axialDrag = null;
    FlyingController flyingBlock = null;
    BalloonController balloonBlock = null;
    public float rigidDragMagnitude = 0.05f;
    public Vector3 axialDragMagnitude = new Vector3();
    public float forceMagnitude = 1f;
    public float balloonMagnitude = 1f;
    public float airDensity = 1f;
    public float gravity = 1f;

    ForceBehaviour[] behaviours = null;

    // Use this for initialization
    void Start() {
        rigidbody = this.gameObject.GetComponent<Rigidbody>();
        axialDrag = this.gameObject.GetComponent<AxialDrag>();
        flyingBlock = this.gameObject.GetComponent<FlyingController>();
        balloonBlock = this.gameObject.GetComponent<BalloonController>();
        if (rigidbody) rigidDragMagnitude = rigidbody.drag;
        if (axialDrag) axialDragMagnitude = axialDrag.AxisDrag;
        if (flyingBlock) forceMagnitude = flyingBlock.SpeedSlider.Value;
        if (balloonBlock) balloonMagnitude = balloonBlock.BuoyancySlider.Value;
        foreach (Transform t in this.transform) {
            t.gameObject.AddComponent<ForceBehaviour>();
        }
    }

    void FixedUpdate() {
        if (behaviours == null) behaviours = this.gameObject.GetComponents<ForceBehaviour>();
        if (behaviours == null 
            || (behaviours.Length > 0 && behaviours[0] != this) 
            || (!axialDrag && !rigidbody && !flyingBlock && ! balloonBlock)) {
            Destroy(this);
            return;
        }
        if (!StatMaster.levelSimulating) return;

        airDensity = (float) (0.5f + 0.5f*System.Math.Tanh((4 / AtmosphericsController.AirRange) 
            * (this.gameObject.transform.position.y 
                - (AtmosphericsController.EndAtmosphereAltitude + AtmosphericsController.AirRange / 2f))));
        gravity = (float)(0.5f + 0.5f * System.Math.Tanh((4 / AtmosphericsController.GravityRange)
            * (this.gameObject.transform.position.y
                - (AtmosphericsController.EndGravityAltitude + AtmosphericsController.GravityRange / 2f))));
        Air(airDensity);
        Gravity(gravity);
    }

    void Air(float density) {
        if (!AtmosphericsController.AirFalloff) return;
        if (rigidbody) rigidbody.drag = rigidDragMagnitude * (1f - density);
        if (axialDrag) axialDrag.AxisDrag = axialDragMagnitude * (1f - density);
        if (flyingBlock) flyingBlock.SpeedSlider.Value = forceMagnitude * (1f - density);
        if (balloonBlock) balloonBlock.BuoyancySlider.Value = balloonMagnitude * (1f - density);
    }

    void Gravity(float gravity) {
        if (!rigidbody || rigidbody.IsSleeping() || !AtmosphericsController.GravityFalloff 
            || StatMaster.GodTools.GravityDisabled) return;
        Vector3 force = rigidbody.mass * (-Physics.gravity * gravity);
        AtmosphericsController.gravityWells.ForEach(x => force += x.Gravity(this.gameObject));
        rigidbody.AddForce(force);
    }
}

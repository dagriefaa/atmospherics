﻿using UnityEngine;
using System.Collections;

public class GravityWellBehaviour : MonoBehaviour {

    public bool enabled = true;
    public float radius = 10f;
    public float acceleration = UnityEngine.Physics.gravity.magnitude;

    public static float cutoff { get; private set; } = 0.05f;

    // Use this for initialization
    void Start() {
        AtmosphericsController.gravityWells.Add(this);
    }

    void OnDestroy() {
        AtmosphericsController.gravityWells.Remove(this);
    }

    public Vector3 Gravity(GameObject target) {
        if (!enabled) return Vector3.zero;
        Vector3 toThis = this.gameObject.transform.position - target.gameObject.transform.position;
        toThis /= radius;
        if (toThis.sqrMagnitude > 1) return toThis.normalized * acceleration;
        if (1f / toThis.sqrMagnitude < cutoff) return Vector3.zero;
        return toThis.normalized * (acceleration / toThis.sqrMagnitude);
    }
}

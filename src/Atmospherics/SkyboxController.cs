﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Modding;
using Modding.Levels;

public class SkyboxController : MonoBehaviour {

    public Color SkyboxTint {
        get {
            return (RenderSettings.skybox) ? RenderSettings.skybox.GetColor("_Tint") : Color.clear;
        }
        set {
            RenderSettings.skybox?.SetColor("_Tint", value);
            DynamicGI.UpdateEnvironment();
            //LevelData.Write("Atmo::SkyboxTint", value);
        }
    }
    public float SkyboxRotation {
        get {
            return (RenderSettings.skybox) ? RenderSettings.skybox.GetFloat("_Rotation") : 0f;
        }
        set {
            RenderSettings.skybox?.SetFloat("_Rotation", value);
            DynamicGI.UpdateEnvironment();
            //LevelData.Write("Atmo::SkyboxRotation", value);
        }
    }
    public float SkyboxExposure {
        get {
            return (RenderSettings.skybox) ? RenderSettings.skybox.GetFloat("_Exposure") : 0f;
        }
        set {
            RenderSettings.skybox.SetFloat("_Exposure", value);
            DynamicGI.UpdateEnvironment();
            //LevelData.Write("Atmo::SkyboxExposure", value);
        }
    }
    public float AmbientIntensity {
        get {
            return RenderSettings.ambientIntensity;
        }
        set {
            RenderSettings.ambientIntensity = value;
            DynamicGI.UpdateEnvironment();
            //LevelData.Write("Atmo::AmbientIntensity", value);
        }
    }

    public Dictionary<string, Cubemap> skyboxTextures { get; } = new Dictionary<string, Cubemap>();
    public string currentKey { get; private set; } = "null";
    Material skyMaterial;
    
    // Use this for initialization
    void Start() {
        UnityEngine.Object.DontDestroyOnLoad(this.gameObject);
        ModAssetBundle stuff = ModResource.GetAssetBundle("sky");
        skyMaterial = new Material(Shader.Find("Skybox/Cubemap"));
        stuff.AssetBundle.LoadAllAssets<Cubemap>().ToList().ForEach(x => skyboxTextures.Add(x.name, x));
    }

    public void SetSkybox(string key) {
        if (key.Equals("null")) {
            currentKey = key;
            Camera.main.clearFlags = CameraClearFlags.Color;
            RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Flat;
            DynamicGI.UpdateEnvironment();
            //LevelData.Write("Atmo::SkyboxName", key);
            return;
        }
        foreach (String s in skyboxTextures.Keys) {
            if (s.StartsWith(key)) {
                currentKey = key;
                Camera.main.clearFlags = CameraClearFlags.Skybox;
                RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Skybox;
                skyMaterial.SetTexture("_Tex", skyboxTextures[s]);
                RenderSettings.skybox = skyMaterial;
                DynamicGI.UpdateEnvironment();
                //LevelData.Write("Atmo::SkyboxName", s);
                break;
            }
        }
    }

    public void Load() {
        if (AtmosphericsController.LevelData.HasKey("Atmo::SkyboxName"))
            SetSkybox(AtmosphericsController.LevelData.ReadString("Atmo::SkyboxName"));

        if (AtmosphericsController.LevelData.HasKey("Atmo::SkyboxTint"))
            SkyboxTint = AtmosphericsController.LevelData.ReadColor("Atmo::SkyboxTint");

        if (AtmosphericsController.LevelData.HasKey("Atmo::SkyboxRotation"))
            SkyboxRotation = AtmosphericsController.LevelData.ReadFloat("Atmo::SkyboxRotation");

        if (AtmosphericsController.LevelData.HasKey("Atmo::SkyboxExposure"))
            SkyboxExposure = AtmosphericsController.LevelData.ReadFloat("Atmo::SkyboxExposure");

        if (AtmosphericsController.LevelData.HasKey("Atmo::AmbientIntensity"))
            AmbientIntensity = AtmosphericsController.LevelData.ReadFloat("Atmo::AmbientIntensity");
    }

}

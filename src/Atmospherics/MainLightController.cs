﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Modding;

public class MainLightController : MonoBehaviour {

    public static Dictionary<String, Flare> LensFlares = null;
    public static Light mainLight = null;
    static int status = 0;

    // Use this for initialization
    void Start() {
        ModAssetBundle stuff = ModResource.GetAssetBundle("flares");
        LensFlares = new Dictionary<String, Flare>();
        LensFlares.Add("null", null);
        stuff.AssetBundle.LoadAllAssets<Flare>().ToList().ForEach(x => LensFlares.Add(x.name, x));
        DontDestroyOnLoad(this.gameObject);
        SceneManager.sceneLoaded += (x, y) => this.enabled = true;
    }

    void Update() {
        if (!Camera.main.gameObject.GetComponent<FlareLayer>()) {
            Camera.main.gameObject.AddComponent<FlareLayer>();
        }
        if (!mainLight) {
            if (status++ == 0) {
                return;
            }
            mainLight = GameObject.Find("Directional light").GetComponent<Light>();
            if (!mainLight) {
                this.enabled = false;
            }
        }
    }

    public static void SetFlare(string key) {
        if (key.Equals("null")) {
            mainLight.flare = null;
            //LevelData.Write("Atmo::LightFlare", key);
            return;
        }
        foreach (string s in LensFlares.Keys) {
            if (s.StartsWith(key)) {
                mainLight.flare = LensFlares[key];
                //LevelData.Write("Atmo::LightFlare", s);
                break;
            }
        }
    }

    public static Vector3 Rotation {
        get { return mainLight.transform.eulerAngles; }
        set {
            mainLight.transform.eulerAngles = value;
            //LevelData.Write("Atmo::LightRotation", value);
        }
    }

    public static Color Color {
        get { return mainLight.color; }
        set {
            mainLight.color = value;
            //LevelData.Write("Atmo::LightColor", value);
        }
    }

    public static float Intensity {
        get { return mainLight.intensity; }
        set {
            mainLight.intensity = value;
            //LevelData.Write("Atmo::LightIntensity", value);
        }
    }

    public static Color AmbientColor {
        get { return RenderSettings.ambientLight; }
        set {
            RenderSettings.ambientLight = value;
            //LevelData.Write("Atmo::LightAmbient", value);
        }
    }

    public static void Load() {
        if (AtmosphericsController.LevelData.HasKey("Atmo::LightRotation"))
            mainLight.transform.rotation = Quaternion.Euler(AtmosphericsController.LevelData.ReadVector3("Atmo::LightRotation"));
        if (AtmosphericsController.LevelData.HasKey("Atmo::LightFlare"))
            SetFlare(AtmosphericsController.LevelData.ReadString("Atmo::LightFlare"));
        if (AtmosphericsController.LevelData.HasKey("Atmo::LightColor"))
            mainLight.color = AtmosphericsController.LevelData.ReadColor("Atmo::LightColor");
        if (AtmosphericsController.LevelData.HasKey("Atmo::LightIntensity"))
            mainLight.intensity = AtmosphericsController.LevelData.ReadFloat("Atmo::LightIntensity");
        if (AtmosphericsController.LevelData.HasKey("Atmo::LightAmbient"))
            RenderSettings.ambientLight = AtmosphericsController.LevelData.ReadColor("Atmo::LightAmbient");

    }
}

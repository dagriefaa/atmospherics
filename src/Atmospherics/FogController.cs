﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FogController : MonoBehaviour {

    bool sceneChanged = true;
    static ColorfulFog barrenFog = null;
    static GameObject fogSphere = null;
    static GameObject starSphere = null;

    public static Color FogColor {
        get {
            if (!OpenFogVolume.CurrentActive) return Color.black;
            return OpenFogVolume.CurrentActive.FogColor; }
        set {
            if (!OpenFogVolume.CurrentActive) return;
            OpenFogVolume.CurrentActive.FogColor = value;
            BarrenEnvToggle(false);
        }
    }
    public static float Visibility {
        get {
            if (!OpenFogVolume.CurrentActive) return -1f;
            return OpenFogVolume.CurrentActive.Visibility;
        }
        set {
            if (!OpenFogVolume.CurrentActive) return;
            OpenFogVolume.CurrentActive.Visibility = value;
            BarrenEnvToggle(false);
        }
    }
    public static float FogStartDistance {
        get {
            if (!OpenFogVolume.CurrentActive) return -1f;
            return OpenFogVolume.CurrentActive.FogStartDistance;
        }
        set {
            if (!OpenFogVolume.CurrentActive) return;
            OpenFogVolume.CurrentActive.FogStartDistance = value;
            BarrenEnvToggle(false);
        }
    }
    public static bool EnableInscattering {
        get {
            if (!OpenFogVolume.CurrentActive) return false;
            return OpenFogVolume.CurrentActive.EnableInscattering;
        }
        set {
            if (!OpenFogVolume.CurrentActive) return;
            OpenFogVolume.CurrentActive.EnableInscattering = value;
            BarrenEnvToggle(false);
        }
    }
    public static Vector3 InscatteringRotation {
        get {
            if (!OpenFogVolume.CurrentActive) return Vector3.zero;
            return OpenFogVolume.CurrentActive.InscatteringRotation;
        }
        set {
            if (!OpenFogVolume.CurrentActive) return;
            OpenFogVolume.CurrentActive.InscatteringRotation = value;
            BarrenEnvToggle(false);
        }
    }
    public static Color InscatteringColor {
        get {
            if (!OpenFogVolume.CurrentActive) return Color.black;
            return OpenFogVolume.CurrentActive.InscatteringColor;
        }
        set {
            if (!OpenFogVolume.CurrentActive) return;
            OpenFogVolume.CurrentActive.InscatteringColor = value;
            BarrenEnvToggle(false);
        }
    }
    public static float InscatteringExponent {
        get {
            if (!OpenFogVolume.CurrentActive) return -1f;
            return OpenFogVolume.CurrentActive.InscatteringExponent;
        }
        set {
            if (!OpenFogVolume.CurrentActive) return;
            OpenFogVolume.CurrentActive.InscatteringExponent = value;
            BarrenEnvToggle(false);
        }
    }
    public static float InscatteringIntensity {
        get {
            if (!OpenFogVolume.CurrentActive) return -1f;
            return OpenFogVolume.CurrentActive.InscatteringIntensity;
        }
        set {
            if (!OpenFogVolume.CurrentActive) return;
            OpenFogVolume.CurrentActive.InscatteringIntensity = value;
            BarrenEnvToggle(false);
        }
    }
    public static float InscatteringStartDistance {
        get {
            if (!OpenFogVolume.CurrentActive) return -1f;
            return OpenFogVolume.CurrentActive.InscatteringStartDistance;
        }
        set {
            if (!OpenFogVolume.CurrentActive) return;
            OpenFogVolume.CurrentActive.InscatteringStartDistance = value;
            BarrenEnvToggle(false);
        }
    }
    public static float InscatteringTransitionWideness {
        get {
            if (!OpenFogVolume.CurrentActive) return -1f;
            return OpenFogVolume.CurrentActive.InscatteringTransitionWideness;
        }
        set {
            if (!OpenFogVolume.CurrentActive) return;
            OpenFogVolume.CurrentActive.InscatteringTransitionWideness = value;
            BarrenEnvToggle(false);
        }
    }

    public static void Reset() {
        OpenFogVolume.CurrentActive?.Reset();
        BarrenEnvToggle(true);
    }

    public static void Load() {
        OpenFogVolume.CurrentActive?.Load();
    }

    public void Start() {
        DontDestroyOnLoad(this.gameObject);
        SceneManager.sceneLoaded += (x, y) => sceneChanged = true;
    }

    public void Update() {
        if (sceneChanged) {
            sceneChanged = false;
            Resources.FindObjectsOfTypeAll<FogVolume>().ToList().ForEach(x => {
                if (x.hideFlags == HideFlags.None && !x.GetComponent<OpenFogVolume>()) x.gameObject.AddComponent<OpenFogVolume>();
            });
            Resources.FindObjectsOfTypeAll<FogVolumeInscatterX2>().ToList().ForEach(x => {
                if (x.hideFlags == HideFlags.None && !x.GetComponent<OpenFogVolume>()) x.gameObject.AddComponent<OpenFogVolume>();
            });
            if (GameObject.Find("LEVEL BARREN EXPANSE") || GameObject.Find("LEVEL SANDBOX")) {
                barrenFog = Camera.main.gameObject.GetComponent<ColorfulFog>();
                fogSphere = Camera.main.gameObject.transform.FindChild("FOG SPHERE")?.gameObject;
                starSphere = GameObject.Find("STAR SPHERE");
            }
        }
    }

    public static void BarrenEnvToggle(bool toggle) {
        if (barrenFog) barrenFog.enabled = toggle;
        fogSphere?.SetActive(toggle);
        starSphere?.SetActive(toggle);
    }
}

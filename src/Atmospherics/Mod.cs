using System;
using Modding;
using UnityEngine;

namespace Atmospherics
{
	public class Mod : ModEntryPoint
	{
		public override void OnLoad()
		{
            //Events.OnBlockInit += x => x.GameObject.AddComponent<ForceBehaviour>();
            GameObject atmoController = new GameObject("Atmospherics Controller");
            //atmoController.AddComponent<AtmosphericsController>();
            atmoController.AddComponent<MainLightController>();
            atmoController.AddComponent<SkyboxController>();
            atmoController.AddComponent<FogController>();
            atmoController.AddComponent<Mapper>();
        }
    }
}

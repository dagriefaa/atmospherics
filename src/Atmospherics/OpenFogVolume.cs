﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000002 RID: 2
[ExecuteInEditMode]
public class OpenFogVolume : MonoBehaviour {

    Color inscatteringColor = Color.white;
    Color fogColor = new Color(0.5f, 0.6f, 0.7f, 1f);
    Light Sun = null;
    float visibility = 5f;
    float fogStartDistance = 40f;
    bool enableInscattering = true;
    float inscatteringExponent = 15f;
    float inscatteringIntensity = 2f;
    float inscatteringStartDistance = 400f;
    float inscatteringTransitionWideness = 1f;

    public Color FogColor {
        get { return fogColor; }
        set {
            fogColor = value;
            OnEnable();
            //LevelData.Write("Atmo::FogColor", value);
        }
    }
    public float Visibility {
        get { return visibility; }
        set {
            visibility = value;
            OnEnable();
            //LevelData.Write("Atmo::FogVisibility", value);
        }
    }
    public float FogStartDistance {
        get { return fogStartDistance; }
        set {
            fogStartDistance = value;
            OnEnable();
            //LevelData.Write("Atmo::FogStartDistance", value);
        }
    }
    public bool EnableInscattering {
        get { return enableInscattering; }
        set {
            enableInscattering = value;
            OnEnable();
            //LevelData.Write("Atmo::FogEnableInscattering", value);
        }
    }
    public Vector3 InscatteringRotation {
        get { return Sun.transform.rotation.eulerAngles; }
        set {
            Sun.transform.rotation = Quaternion.Euler(value);
            OnEnable();
            //LevelData.Write("Atmo::FogInscatteringRotation", value);
        }
    }
    public Color InscatteringColor {
        get { return inscatteringColor; }
        set {
            inscatteringColor = value;
            OnEnable();
            //LevelData.Write("Atmo::FogInscatteringColor", value);
        }
    }
    public float InscatteringExponent {
        get { return inscatteringExponent; }
        set {
            inscatteringExponent = value;
            OnEnable();
            //LevelData.Write("Atmo::FogInscatteringExponent", value);
        }
    }
    public float InscatteringIntensity {
        get { return inscatteringIntensity; }
        set {
            inscatteringIntensity = value;
            OnEnable();
            //LevelData.Write("Atmo::FogInscatteringIntensity", value);
        }
    }
    public float InscatteringStartDistance {
        get { return inscatteringStartDistance; }
        set {
            inscatteringStartDistance = value;
            OnEnable();
            //LevelData.Write("Atmo::FogInscatteringStartDistance", value);
        }
    }
    public float InscatteringTransitionWideness {
        get { return inscatteringTransitionWideness; }
        set {
            inscatteringTransitionWideness = value;
            OnEnable();
            //LevelData.Write("Atmo::FogInscatteringTransitionWideness", value);
        }
    }

    public int initialised = 0;
    FogVolume original = null;
    FogVolumeInscatterX2 originalX2 = null;

    public static OpenFogVolume CurrentActive { get; private set; } = null;
    public Material FogMaterial { get; private set; } = null;

    bool Initialise() {
        Resources.FindObjectsOfTypeAll<Light>().ToList().ForEach(
            x => {
                if (x.gameObject.name.Equals("Directional light Fog")) this.Sun = x;
            });
        if (!this.Sun) {
            Debug.Log("OpenFogVolume: Did not find sun light!");
            this.enabled = false;
            return false;
        }
        Renderer thisRenderer = this.gameObject.GetComponent<Renderer>();

        this.FogColor = thisRenderer.materials[0].color;
        this.InscatteringColor = thisRenderer.materials[0].GetColor("_InscatteringColor");

        original = gameObject.GetComponent<FogVolume>();
        if (original) {
            this.Visibility = original.Visibility;
            this.InscatteringExponent = original.InscateringExponent;
            this.InscatteringIntensity = original.InscatteringIntensity;
            this.InscatteringStartDistance = original.InscatteringStartDistance;
            this.InscatteringTransitionWideness = original.InscatteringTransitionWideness;
            this._3DNoiseScale = original._3DNoiseScale;
            this._3DNoiseStepSize = original._3DNoiseStepSize;
            this.FogStartDistance = original.fogStartDistance;
            this._NoiseVolume = original._NoiseVolume;
            this.Quality = (QualityLevel)original.Quality;
            this.NoiseIntensity = original.NoiseIntensity;
            this.NoiseContrast = original.NoiseContrast;
            this.Speed = original.Speed;
            this.Stretch = original.Stretch;
            original.enabled = false;
        } else {
            originalX2 = gameObject.GetComponent<FogVolumeInscatterX2>();
            if (originalX2) {
                this.Visibility = originalX2.Visibility;
                this.InscatteringExponent = originalX2.InscateringExponent;
                this.InscatteringIntensity = originalX2.InscatteringIntensity;
                this.InscatteringStartDistance = originalX2.InscatteringStartDistance;
                this.InscatteringTransitionWideness = originalX2.InscatteringTransitionWideness;
                this._3DNoiseScale = originalX2._3DNoiseScale;
                this._3DNoiseStepSize = originalX2._3DNoiseStepSize;
                this.FogStartDistance = originalX2.fogStartDistance;
                this._NoiseVolume = originalX2._NoiseVolume;
                this.Quality = (QualityLevel)originalX2.Quality;
                this.NoiseIntensity = originalX2.NoiseIntensity;
                this.NoiseContrast = originalX2.NoiseContrast;
                this.Speed = originalX2.Speed;
                this.Stretch = originalX2.Stretch;
                originalX2.enabled = false;
            }
        }
        this.FogMaterial = this.gameObject.GetComponent<Renderer>().materials[0];
        Debug.Log("OpenFogVolume: Initialised.");
        return true;
    }

    // Token: 0x06000003 RID: 3 RVA: 0x000021DC File Offset: 0x000003DC
    public void OnEnable() {
        if (initialised < 2) return;
        this.ToggleKeyword();
        if (!StatMaster.isHeadless) {
            Camera.main.depthTextureMode |= DepthTextureMode.Depth;
        }
        CurrentActive = this;
    }

    // Token: 0x06000004 RID: 4 RVA: 0x00002270 File Offset: 0x00000470
    public static void Wireframe(GameObject obj, bool Enable) { }

    // Token: 0x06000005 RID: 5 RVA: 0x00002274 File Offset: 0x00000474
    private void Update() {
        if (!this.gameObject.activeInHierarchy || !this.gameObject.activeSelf) return;
        switch (initialised) {
            case 0: initialised++; break;
            case 1:
                Initialise();
                initialised++;
                OnEnable();
                break;
        }
    }

    // Token: 0x06000006 RID: 6 RVA: 0x00002278 File Offset: 0x00000478
    private void OnWillRenderObject() {
        if (initialised < 2) return;
        this.FogMaterial = this.gameObject.GetComponent<Renderer>().materials[0];
        this.FogMaterial.SetColor("_Color", this.FogColor);
        this.FogMaterial.SetColor("_InscatteringColor", this.InscatteringColor);
        this.FogMaterial.SetFloat("FogStartDistance", this.FogStartDistance);
        if (this.Sun) {
            this.FogMaterial.SetFloat("_InscatteringIntensity", this.InscatteringIntensity);
            this.FogMaterial.SetVector("L", -this.Sun.transform.forward);
            this.FogMaterial.SetFloat("_InscateringExponent", this.InscatteringExponent);
            this.FogMaterial.SetFloat("InscatteringTransitionWideness", this.InscatteringTransitionWideness);
        }
        if (this.EnableNoise && this._NoiseVolume) {
            Shader.SetGlobalTexture("_NoiseVolume", this._NoiseVolume);
            this.FogMaterial.SetFloat("gain", this.NoiseIntensity);
            this.FogMaterial.SetFloat("threshold", this.NoiseContrast * 0.5f);
            this.FogMaterial.SetFloat("_3DNoiseScale", this._3DNoiseScale * 0.001f);
            this.FogMaterial.SetFloat("_3DNoiseStepSize", this._3DNoiseStepSize * 0.001f / (float)this.Quality);
            this.FogMaterial.SetVector("Speed", this.Speed);
            this.FogMaterial.SetVector("Stretch", new Vector4(1f, 1f, 1f, 1f) + this.Stretch * 0.01f);
        }
        this.FogMaterial.SetFloat("InscatteringStartDistance", this.InscatteringStartDistance);
        Vector3 localScale = this.gameObject.transform.localScale;
        base.transform.localScale = new Vector3((float)decimal.Round((decimal)localScale.x, 2), localScale.y, localScale.z);
        this.FogMaterial.SetVector("_BoxMin", localScale * -0.5f);
        this.FogMaterial.SetVector("_BoxMax", localScale * 0.5f);
        this.FogMaterial.SetFloat("_Visibility", this.Visibility);
    }

    // Token: 0x06000007 RID: 7 RVA: 0x000024F8 File Offset: 0x000006F8
    private void ToggleKeyword() {
        if (this.EnableNoise) {
            this.FogMaterial.EnableKeyword("_FOG_VOLUME_NOISE");
        }
        else {
            this.FogMaterial.DisableKeyword("_FOG_VOLUME_NOISE");
        }
        if (this.EnableInscattering && this.Sun) {
            this.FogMaterial.EnableKeyword("_FOG_VOLUME_INSCATTERING");
        }
        else {
            this.FogMaterial.DisableKeyword("_FOG_VOLUME_INSCATTERING");
        }
        switch (this.Quality) {
            case QualityLevel.Low:
                this.FogMaterial.EnableKeyword("_LQ");
                this.FogMaterial.DisableKeyword("_MQ");
                this.FogMaterial.DisableKeyword("_HQ");
                break;
            case QualityLevel.Medium:
                this.FogMaterial.EnableKeyword("_MQ");
                this.FogMaterial.DisableKeyword("_LQ");
                this.FogMaterial.DisableKeyword("_HQ");
                break;
            case QualityLevel.High:
                this.FogMaterial.EnableKeyword("_HQ");
                this.FogMaterial.DisableKeyword("_MQ");
                this.FogMaterial.DisableKeyword("_LQ");
                break;
        }
    }

    public void Reset() {
        if (original) original.enabled = true;
        if (originalX2) originalX2.enabled = true;
        initialised = 0;
    }

    public void Load() {
        if (AtmosphericsController.LevelData.HasKey("Atmo::FogColor"))
            FogColor = AtmosphericsController.LevelData.ReadColor("Atmo::FogColor");

        if (AtmosphericsController.LevelData.HasKey("Atmo::FogVisibility"))
            Visibility = AtmosphericsController.LevelData.ReadFloat("Atmo::FogVisibility");

        if (AtmosphericsController.LevelData.HasKey("Atmo::FogStartDistance"))
            FogStartDistance = AtmosphericsController.LevelData.ReadFloat("Atmo::FogStartDistance");

        if (AtmosphericsController.LevelData.HasKey("Atmo::FogEnableInscattering"))
            EnableInscattering = AtmosphericsController.LevelData.ReadBool("Atmo::FogEnableInscattering");

        if (AtmosphericsController.LevelData.HasKey("Atmo::FogInscatteringRotation"))
            InscatteringRotation = AtmosphericsController.LevelData.ReadVector3("Atmo::FogInscatteringRotation");

        if (AtmosphericsController.LevelData.HasKey("Atmo::FogInscatteringColor"))
            InscatteringColor = AtmosphericsController.LevelData.ReadColor("Atmo::FogInscatteringColor");

        if (AtmosphericsController.LevelData.HasKey("Atmo::FogInscatteringExponent"))
            InscatteringExponent = AtmosphericsController.LevelData.ReadFloat("Atmo::FogInscatteringExponent");

        if (AtmosphericsController.LevelData.HasKey("Atmo::FogInscatteringIntensity"))
            InscatteringIntensity = AtmosphericsController.LevelData.ReadFloat("Atmo::FogInscatteringIntensity");

        if (AtmosphericsController.LevelData.HasKey("Atmo::FogInscatteringStartDistance"))
            InscatteringStartDistance = AtmosphericsController.LevelData.ReadFloat("Atmo::FogInscatteringStartDistance");

        if (AtmosphericsController.LevelData.HasKey("Atmo::FogInscatteringTransitionWideness"))
            InscatteringTransitionWideness = AtmosphericsController.LevelData.ReadFloat("Atmo::FogInscatteringTransitionWideness");
    }

    // Token: 0x0400000A RID: 10
    public float _3DNoiseScale = 300f;

    // Token: 0x0400000B RID: 11
    public float _3DNoiseStepSize = 50f;

    // Token: 0x0400000D RID: 13
    public Texture3D _NoiseVolume;

    public enum QualityLevel {
        Low = 1, Medium = 2, High = 3
    }

    // Token: 0x0400000E RID: 14
    [Range(1f, 3f)]
    public QualityLevel Quality = QualityLevel.Low;

    // Token: 0x0400000F RID: 15
    [Range(0f, 10f)]
    public float NoiseIntensity = 1f;

    // Token: 0x04000010 RID: 16
    [Range(0f, 1f)]
    public float NoiseContrast;

    // Token: 0x04000014 RID: 20
    [SerializeField]
    public bool EnableNoise = false;

    // Token: 0x04000015 RID: 21
    public Vector4 Speed = new Vector4(0f, 0f, 0f, 0f);

    // Token: 0x04000016 RID: 22
    public Vector4 Stretch = new Vector4(0f, 0f, 0f, 0f);
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;
using Modding;
using Modding.Levels;
using Modding.Blocks;

public class AtmosphericsController : MonoBehaviour {

    private static float endAtmoAltitude = 500f;
    private static float startAtmoAltitude = 1300f;
    private static float endGravAltitude = 1200f;
    private static float startGravAltitude = 1600f;
    private static bool gravityFalloff = true;
    private static bool airFalloff = true;
    private static bool iceFreeze = false;

    public static float StartAtmosphereAltitude {
        get { return startAtmoAltitude; }
        set {
            startAtmoAltitude = value;
            //LevelData.Write("Atmo::StartAtmosphereAltitude", value);
        }
    }
    public static float EndAtmosphereAltitude {
        get { return endAtmoAltitude; }
        set {
            endAtmoAltitude = value;
            //LevelData.Write("Atmo::EndAtmosphereAltitude", value);
        }
    }
    public static float StartGravityAltitude {
        get { return startGravAltitude; }
        set {
            startGravAltitude = value;
            //LevelData.Write("Atmo::StartGravityAltitude", value);
        }
    }
    public static float EndGravityAltitude {
        get { return endGravAltitude; }
        set {
            endGravAltitude = value;
            //LevelData.Write("Atmo::EndGravityAltitude", value);
        }
    }
    public static bool GravityFalloff {
        get { return gravityFalloff; }
        set {
            gravityFalloff = value;
            //LevelData.Write("Atmo::GravityFalloff", value);
        }
    }
    public static bool AirFalloff {
        get { return airFalloff; }
        set {
            airFalloff = value;
            //LevelData.Write("Atmo::AirFalloff", value);
        }
    }
    public static bool IceFreeze {
        get { return iceFreeze; }
        set {
            iceFreeze = value;
            //LevelData.Write("Atmo::IceFreeze", value);
        }
    }

    public static float AirDensity { get; private set; }
    public static float Gravity { get; private set; }

    public static float AirRange { get { return (startAtmoAltitude - endAtmoAltitude); } }
    public static float GravityRange { get { return (startGravAltitude - endGravAltitude); } }
    public static XDataHolder LevelData { get { return Level.GetCurrentLevel().CustomData; } }

    public static List<GravityWellBehaviour> gravityWells = new List<GravityWellBehaviour>();

    GameObject iceLayer = null;
    ForceBehaviour machine = null;
    bool sceneChanged = true;

    // Use this for initialization
    void Start() {
        DontDestroyOnLoad(this.gameObject);
        SceneManager.sceneLoaded += (x, y) => sceneChanged = true;
    }
    
    // Update is called once per frame
    void Update() {

        if (sceneChanged) {
            sceneChanged = false;
            Resources.FindObjectsOfTypeAll<FogVolume>().ToList().ForEach(x => {
                if (x.hideFlags == HideFlags.None && !x.GetComponent<OpenFogVolume>()) x.gameObject.AddComponent<OpenFogVolume>();
            });
            Resources.FindObjectsOfTypeAll<FogVolumeInscatterX2>().ToList().ForEach(x => {
                if (x.hideFlags == HideFlags.None && !x.GetComponent<OpenFogVolume>()) x.gameObject.AddComponent<OpenFogVolume>();
            });
            iceLayer = GameObject.Find("ICE FREEZE");
        }
        iceLayer?.SetActive(AtmosphericsController.iceFreeze);

        GetMachineDensity();
    }

    void GetMachineDensity() {
        if (PlayerMachine.GetLocal() == null || PlayerMachine.GetLocal().InternalObject == null ||
            !PlayerMachine.GetLocal().InternalObject.isSimulating) return;
        if (!machine) {
            try {
                if (Game.IsSimulating && !Game.IsSpectator && PlayerMachine.GetLocal().SimulationMachine.transform.childCount > 0) {
                    machine = PlayerMachine.GetLocal().SimulationMachine.transform.GetChild(0).gameObject.GetComponent<ForceBehaviour>();
                }
            } catch (Exception ex) {
                Debug.LogError(string.Format("[Accelerometer] {0}", ex.Message));
            }
        }
        Gravity = machine.gravity;
        AirDensity = machine.airDensity;
    }

    public void Load() {
        if (AtmosphericsController.LevelData.HasKey("Atmo::GravityFalloff"))
            gravityFalloff = AtmosphericsController.LevelData.ReadBool("Atmo::GravityFalloff");

        if (AtmosphericsController.LevelData.HasKey("Atmo::AirFalloff"))
            airFalloff = AtmosphericsController.LevelData.ReadBool("Atmo::AirFalloff");

        if (AtmosphericsController.LevelData.HasKey("Atmo::IceFreeze"))
            iceFreeze = AtmosphericsController.LevelData.ReadBool("Atmo::IceFreeze");

        if (AtmosphericsController.LevelData.HasKey("Atmo::EndAtmosphereAltitude"))
            endAtmoAltitude = AtmosphericsController.LevelData.ReadFloat("Atmo::EndAtmosphereAltitude");

        if (AtmosphericsController.LevelData.HasKey("Atmo::StartAtmosphereAltitude"))
            startAtmoAltitude = AtmosphericsController.LevelData.ReadFloat("Atmo::StartAtmosphereAltitude");
    }
}
